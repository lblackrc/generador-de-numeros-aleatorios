#!/home/luis/anaconda3/envs/hackspace/bin/python

import PySimpleGUI as sg
from parametros import *

# aplicando condicion para habilitar la ejecucion de interfaz grafica en caso de errores
import os
if os.environ.get('DISPLAY', '') == '':
    print('no display found. Using :0.0')
    os.environ.__setitem__('DISPLAY', ':0.0')

# funcion generadora de numeros aleatorios
def generarRandom(a, b, semilla, m):
    ri = (a + b * semilla) % m
    Ri = ri / (m-1)
    global r0    
    r0 = ri  # se establece una nueva semilla
    return round(Ri, 5)

# verificando si hay semillas guardadas de una anterior ejecucion del programa
try:
    fileHandler = open("semillas.txt",'r+')  
    contenido = fileHandler.read()
    if len(contenido)>0:
        r0 = int(contenido.strip())
    print("semilla guardada anteriormente:", contenido)
except:
    # en caso el archivo semillas.txt no exista, se crea el archivo
    fileHandler = open("semillas.txt",'w+')
    contenido = ""

fileHandler.close()

# solo en la primera iteracion se hara la lectura del archivo de semillas
randomCalculado = False

# Comienza el codigo propio para la interfaz
sg.theme('DarkAmber')  # Tema de la ventana

# Elementos que tendra la ventana
layout = [[sg.Text('Valor mínimo'), sg.In(enable_events=True, key="-MINIMO-")],
          [sg.Text('Valor máximo'), sg.In(enable_events=True, key="-MAXIMO-")],
          [sg.Text('Número aleatorio generado:'), sg.In(size=(33, 1), key="-RANDOM-")],
          [sg.Button('Generar'), sg.Button('Salir')],
        ]

# Generando la ventana
window = sg.Window('Generador de números aleatorios', layout, location=(600,600))

# Bucle para procesar los eventos que ocurren en la ventana y obtener sus valores
while True:
    event, values = window.read()
    if event == 'Salir' or event == sg.WIN_CLOSED:  # Si el usuario cierra la ventana o hace click en 'Salir'
        break

    elif event == "-MINIMO-":
        try:
            minimoValor = int(values["-MINIMO-"])
            print("Valor minimo ingresado",minimoValor)
        except:
            window["-MINIMO-"].update("")

    elif event == "-MAXIMO-":
        try:
            maximoValor = int(values["-MAXIMO-"])
            print("Valor maximo ingresado",maximoValor)
        except:
            window["-MAXIMO-"].update("")

    elif event == "Generar":
        print("He presionado generar")
        try:
            if ('minimoValor' in globals()) and ('maximoValor' in globals()): 
                print("he entrado al if")
                numeroRandom = generarRandom(a, b, r0, m)

            x = minimoValor + numeroRandom * (maximoValor - minimoValor)
            randomCalculado = True     # indicamos que el numero aleatorio se ha calculado
            print("He calculado el random")
            print("Numero aleatorio generado:",round(x,5))
            window["-RANDOM-"].update(round(x,5))
        except:
            # no se han ingresado los valores minimo y maximo, se continua
            continue

window.close()  # cerrar la ventana es importante para que no se ejecute en segundo plano

try:
    # si hemos calculado al menos un numero aleatorio, debemos guardar su respectiva semilla generadora
    if randomCalculado:
        fileHandler = open("semillas.txt",'w+')
        fileHandler.write(str(r0))
        fileHandler.close()
    print("Valor de r0 guardado actualmente:", r0)
except:
    print("no hagas nada")